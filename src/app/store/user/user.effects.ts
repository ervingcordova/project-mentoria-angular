import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, tap } from 'rxjs/operators';
import { UserActions } from './user.actions';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { CharactersService } from 'src/app/services/characters.service';

@Injectable()
export class UserEffects {
  // action$: Observable que escucha las acciones

  getUsers$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(UserActions.getUsers),
      tap((action) => console.log('[ACTION]', action)),
      mergeMap(() =>
        // Disparamos al servicio
        this.characterS.getCharacaters().pipe(
          tap((data) => console.log('[DATA DEL SERVICIO]', data)),
          map((list) => {
            return UserActions.getUsersSuccess({ list: Object.values(list) });
          })
        )
      )
    )
  );

  uploadImage$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(UserActions.uploadImage),
      tap((action) => console.log('[ACTION]', action)),
      mergeMap(() =>
        // Disparamos al servicio
        this.characterS.getCharacaters().pipe(
          tap((data) => console.log('[DATA DEL SERVICIO]', data)),
          map((list) => {
            return UserActions.getUsersSuccess({ list: Object.values(list) });
          })
        )
      )
    )
  );

  constructor(
    private action$: Actions,
    private characterS: CharactersService
  ) {}
}
