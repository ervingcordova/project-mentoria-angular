import { createFeatureSelector, createSelector } from '@ngrx/store';

export const selectorUser = createFeatureSelector<any>('usuario');

export const id = createSelector(selectorUser, (state) => state.id);

export const list = createSelector(selectorUser, (state) =>
  Object.values(state.entities)
);
