import { UserActions } from './user.actions';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as UsersSelectors from './user.selectors';

@Injectable()
export class UserFacade {
  id$ = this.store.select(UsersSelectors.id);
  list$ = this.store.select(UsersSelectors.list);

  constructor(private store: Store<any>) {}

  increment(): void {
    this.store.dispatch(UserActions.incrementUserId());
  }
  decrement(): void {
    this.store.dispatch(UserActions.decrementUserId());
  }
  getUserList(): void {
    this.store.dispatch(UserActions.getUsers());
  }
}
