import { UserActions } from './user.actions';
import { createReducer, on } from '@ngrx/store';
import { state } from '@angular/animations';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { ICharacter } from 'src/app/interfaces/users/character.interface';

const userAdapter = createEntityAdapter<ICharacter>({
  selectId: (character: ICharacter) => character.id,
});

export interface UserState extends EntityState<any> {
  id: number;
  users: ICharacter[];
  loading: boolean;
  selectedUser: ICharacter;
}

export const initialState: UserState = userAdapter.getInitialState({
  id: 20,
  ids: [],
  entities: {},
  users: [],
  selectedUser: { id: 0 },
  loading: false,
});

const _userReducer = createReducer(
  initialState,
  on(UserActions.incrementUserId, (state: UserState) => ({
    ...state,
    id: state.id + 1,
  })),
  on(UserActions.decrementUserId, (state: UserState) => ({
    ...state,
    id: state.id - 1,
  })),
  on(UserActions.getUsers, (state: UserState) => ({
    ...state,
    loading: true,
  })),
  on(UserActions.getUsersSuccess, (state: UserState, { list }) =>
    userAdapter.upsertMany(list, {
      ...state,
      loading: false,
    })
  )
);

export function userReducer(state: any, action: any) {
  return _userReducer(state, action);
}
