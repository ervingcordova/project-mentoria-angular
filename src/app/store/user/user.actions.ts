import { createAction, props } from '@ngrx/store';
import { ICharacter } from 'src/app/interfaces/users/character.interface';

enum UserActionsType {
  GET_USERS = '[USER] GET USERS',
  GET_USERS_SUCCESS = '[USER] GET USERS SUCCESS',
  SAVE_USER_ID = '[USER] SAVE USER ID',
  INCREMENET_USER_ID = '[USER] INCREMENT USER ID',
  DECREMENT_USER_ID = '[USER] DECREMENT USER ID',
  UPLOAD_IMAGE = '[USER] UPLOAD IMAGE',
}

export const UserActions = {
  getUsers: createAction(UserActionsType.GET_USERS),
  getUsersSuccess: createAction(
    UserActionsType.GET_USERS_SUCCESS,
    props<{ list: ICharacter[] }>()
  ),
  saveUserId: createAction(UserActionsType.SAVE_USER_ID),
  incrementUserId: createAction(UserActionsType.INCREMENET_USER_ID),
  decrementUserId: createAction(UserActionsType.DECREMENT_USER_ID),
  uploadImage: createAction(
    UserActionsType.UPLOAD_IMAGE,
    props<{ picture: string }>()
  ),
};
