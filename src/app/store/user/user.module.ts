import { UserEffects } from './user.effects';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { userReducer } from './user.reducer';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('usuario', userReducer),
    EffectsModule.forRoot([UserEffects]),
  ],
})
export class UserStoreModule {}
