export interface ICharacter {
  id: number;
  name?: string;
  image?: string;
  status?: string;
  url?: string;
  gender?: string;
}
