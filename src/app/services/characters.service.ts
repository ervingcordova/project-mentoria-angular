import { ICharacter } from './../interfaces/users/character.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CharactersService {
  constructor(private http: HttpClient) {}

  getCharacaters(): Observable<ICharacter[]> {
    return this.http.get('https://rickandmortyapi.com/api/character').pipe(
      map((response: any) => {
        const { results } = response;

        return results;
      })
    );
  }

  getCharacterById(id: any): Observable<ICharacter> {
    return this.http
      .get(`https://rickandmortyapi.com/api/character/${id}`)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }
}

//https://rickandmortyapi.com/api/character/hola'
