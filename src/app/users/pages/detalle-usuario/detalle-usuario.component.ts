import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from 'src/app/services/characters.service';

@Component({
  selector: 'app-detalle-usuario',
  templateUrl: './detalle-usuario.component.html',
  styleUrls: ['./detalle-usuario.component.css'],
})
export class DetalleUsuarioComponent implements OnInit {
  idCharacter: any = '';
  characterDetail: any;

  constructor(
    private router: ActivatedRoute,
    private characterS: CharactersService
  ) {}

  ngOnInit(): void {
    this.idCharacter = this.router.snapshot.paramMap.get('id');

    this.characterS.getCharacterById(this.idCharacter).subscribe((data) => {
      // console.log(data);
      this.characterDetail = data;
    });
  }
}
