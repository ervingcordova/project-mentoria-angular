import { ICharacter } from './../../../interfaces/users/character.interface';
import { Component, OnInit } from '@angular/core';
import { CharactersService } from 'src/app/services/characters.service';
import { UserFacade } from 'src/app/store/user/user.facade';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css'],
})
export class ListadoComponent implements OnInit {
  charactersList: any = [];
  list$ = this.userFacade.list$;
  name = 'Erving';
  options = [
    {
      value: 'test',
      name: 'test',
    },
    {
      value: 'nombre',
      name: 'nombre',
    },
  ];

  idUser: any;

  constructor(private userFacade: UserFacade) {}

  ngOnInit(): void {
    this.userFacade.getUserList();
    this.userFacade.id$.subscribe((id) => (this.idUser = id));
    // this.userFacade.list$.subscribe((list) => {
    //   this.charactersList = list;
    // });
  }

  incrementId(): void {
    this.userFacade.increment();
  }

  decrementId(): void {
    this.userFacade.decrement();
  }
}
