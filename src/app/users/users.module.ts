import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ListadoComponent } from './pages/listado/listado.component';
import { DetalleUsuarioComponent } from './pages/detalle-usuario/detalle-usuario.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { CharactersService } from '../services/characters.service';
import { CardCharacterComponent } from './components/card-character/card-character.component';
import { ChlidrenComponent } from './components/chlidren/chlidren.component';

@NgModule({
  declarations: [ListadoComponent, DetalleUsuarioComponent, CardCharacterComponent, ChlidrenComponent],
  imports: [CommonModule, UsersRoutingModule, HttpClientModule],
  providers: [UserService, CharactersService],
})
export class UsersModule {}
