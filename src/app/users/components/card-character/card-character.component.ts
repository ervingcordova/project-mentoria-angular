import { ICharacter } from './../../../interfaces/users/character.interface';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-character',
  templateUrl: './card-character.component.html',
  styleUrls: ['./card-character.component.css'],
})
export class CardCharacterComponent implements OnInit {
  @Input() character: any = { id: 1 };
  @Input() name: string = '';
  @Input() apellido: string = '';
  @Input() options: any = [];

  constructor(private route: Router) {}

  ngOnInit(): void {
    console.log('[INPUT] name: ', this.name);
  }

  goToDetail(): void {
    this.route.navigate(['usuarios/detalle-usuario/', this.character.id]);
  }
}
