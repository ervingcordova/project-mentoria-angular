import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChlidrenComponent } from './chlidren.component';

describe('ChlidrenComponent', () => {
  let component: ChlidrenComponent;
  let fixture: ComponentFixture<ChlidrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChlidrenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChlidrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
