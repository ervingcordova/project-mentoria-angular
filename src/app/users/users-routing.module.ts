import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleUsuarioComponent } from './pages/detalle-usuario/detalle-usuario.component';
import { ListadoComponent } from './pages/listado/listado.component';

const routes: Routes = [
  {
    path: 'lista',
    component: ListadoComponent,
  },
  {
    path: 'detalle-usuario/:id',
    component: DetalleUsuarioComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
